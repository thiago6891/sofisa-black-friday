'''
Script to verify sofisa's black friday promos
'''
import urllib.request
import webbrowser

def run_check():
    '''
    Keep checking for a page refresh until it's done
    '''
    liberated = False
    url = 'https://goo.gl/forms/7ajac885xm7GhddH3'
    test_str = 'valor'

    print('Initiating check...')
    while not liberated:
        file = urllib.request.urlopen(url)
        encoded_page = file.read()
        page = encoded_page.decode('utf8')
        file.close()

        if test_str in page.lower():
            webbrowser.open(url)
            webbrowser.open(url)

            file = open('sofisa_form.html', 'w', encoding='utf8')
            file.write(page)
            file.close()

            liberated = True
    print('Done!')

if __name__ == '__main__':
    run_check()
