'''
Script to verify sofisa's black friday promos
'''
import mechanicalsoup

def run_check():
    '''
    Keep checking for a page refresh until it's done
    '''
    url = 'https://goo.gl/forms/ES31ZC4ddMpgJk9t2'

    print('Initiating...')

    done = False
    while not done:
        browser = mechanicalsoup.StatefulBrowser()
        page = browser.open(url)

        try:
            print('Trying to get form...')
            form = browser.select_form()
            print('Form acquired...')

            entries = []
            start_idx = 0
            while page.text.find('entry.', start_idx) != -1:
                entry_start_idx = page.text.find('entry.', start_idx)
                entry_end_idx = page.text.find('"', entry_start_idx)

                entry = page.text[entry_start_idx: entry_end_idx]
                entries.append(entry)

                start_idx = entry_end_idx

            txt = 'freebirdFormviewerViewItemsRadioLabel'
            s_idx = page.text.rfind(txt)
            s_idx = page.text.find('>', s_idx) + 1
            e_idx = page.text.find('<', s_idx)
            radio_value = page.text[s_idx: e_idx]

            form.set(entries[0], 'thiago6891@gmail.com')
            form.set(entries[1], '10000')
            form.set(entries[2], radio_value)

            browser.submit_selected()

            page = browser.get_current_page()

            file = open('test.html', 'w', encoding='utf8')
            file.write(page.getText())
            file.close()

            browser.close()
            done = True
            print('Done!')
        except mechanicalsoup.utils.LinkNotFoundError:
            browser.close()

if __name__ == '__main__':
    run_check()
