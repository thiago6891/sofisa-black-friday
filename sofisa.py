'''
Script to verify sofisa's black friday promos

Test string for Black Friday Hot Site ===== PRÓXIMO LOTE EM 24/11
Test string for open form button      ===== ACESSE AQUI

'''
import urllib.request
import webbrowser
import mechanicalsoup
import requests

TEST_STR = 'liberação do lote de hoje, 24/11, às 15:30'
ACCESS_STR = 'acesse aqui'

def run_check():
    '''
    Keep checking for a page refresh until it's done
    '''
    liberated = False
    url = 'https://blackfriday.sofisadireto.com.br'

    print('Initiating page check...')
    while not liberated:
        file = urllib.request.urlopen(url)
        encoded_page = file.read()
        page = encoded_page.decode('utf8')
        file.close()

        # Test
        #file = open('sofisa.html', 'r', encoding='utf8')
        #page = file.read()
        #file.close()

        if TEST_STR not in page.lower():
            print('Batch released...')

            access_idx = page.lower().find(ACCESS_STR)

            if access_idx == -1:
                print('ACCESS_STR not found')
                webbrowser.open(url)
                return

            href_idx = page.rfind('href', 0, access_idx)
            quote_idx_1 = page.find('"', href_idx, access_idx) + 1
            quote_idx_2 = page.find('"', quote_idx_1, access_idx)

            form_url = page[quote_idx_1: quote_idx_2]
            print('Form URL: ' + form_url)

            print('Initiating form check...')

            while not liberated:
                browser = mechanicalsoup.StatefulBrowser()

                try:
                    page = browser.open(form_url)
                    form = browser.select_form()

                    print('Retrieving entries...')

                    entries = []
                    start_idx = 0
                    while page.text.find('entry.', start_idx) != -1:
                        entry_start_idx = page.text.find('entry.', start_idx)
                        entry_end_idx = page.text.find('"', entry_start_idx)

                        entry = page.text[entry_start_idx: entry_end_idx]
                        entries.append(entry)

                        start_idx = entry_end_idx
                    
                    if (len(entries) != 3):
                        print('Found ' + str(len(entries)) + ' entries!')
                        webbrowser.open(form_url)
                        return

                    txt = 'freebirdFormviewerViewItemsRadioLabel'
                    s_idx = page.text.rfind(txt)
                    s_idx = page.text.find('>', s_idx) + 1
                    e_idx = page.text.find('<', s_idx)
                    radio_value = page.text[s_idx: e_idx]

                    print('Time value found: ' + radio_value)
                    print('Filling form...')

                    form.set(entries[0], 'thiago6891@gmail.com')
                    form.set(entries[1], '10000')
                    form.set(entries[2], radio_value)

                    browser.submit_selected()

                    page = browser.get_current_page()

                    browser.close()

                    file = open('sofisa_google_form_response.html', 'w', encoding='utf8')
                    file.write(page.getText())
                    file.close()

                    liberated = True
                except mechanicalsoup.utils.LinkNotFoundError:
                    browser.close()
                except requests.exceptions.MissingSchema:
                    print('Invalid form URL!')
                    webbrowser.open(url)
                    browser.close()
                    return
    print('Done!')

if __name__ == '__main__':
    run_check()
